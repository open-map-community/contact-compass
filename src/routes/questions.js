function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[a[i], a[j]] = [a[j], a[i]]
  }
  return a
}

export default shuffle([
  {
    name: 'adultChildSex',
    question: 'Sexual contact between children and adults is harmful.',
    response: 0,
    invert: true,
    part: 0,
  },

  {
    name: 'childConsent',
    question: 'Children cannot consent.',
    response: 0,
    invert: true,
    part: 0,
  },

  {
    name: 'healthyChildrenBehaveSexually',
    question: 'Healthy children do not behave sexually.',
    response: 0,
    invert: true,
    part: 0,
  },

  {
    name: 'childAdultSexLegal',
    question:
      'If it was legal, children and adults could have sex without it being harmful.',
    response: 0,
    invert: false,
    part: 0,
  },

  {
    name: 'childIntelligent',
    question:
      'Children are a lot more intelligent than most people give them credit for.',
    response: 0,
    invert: false,
    part: 0,
  },

  {
    name: 'childChildConsent',
    question:
      'Children can consent to sex with other children but not with adults.',
    response: 0,
    invert: true,
    part: 0,
  },

  {
    name: 'childrenUndersandConsent',
    question:
      'Children would be able to understand consent if it was taught to them from a young age.',
    response: 0,
    invert: false,
    part: 0,
  },
  {
    name: 'csaLawIntervention',
    question:
      'Child sexual abuse requires intervention from law enforcement to prevent further harm.',
    response: 0,
    invert: false,
    part: 1,
  },

  {
    name: 'sexOffenderRegistry',
    question: 'The sex offender registry does more harm than good.',
    response: 0,
    invert: true,
    part: 1,
  },

  {
    name: 'interventionsCPS',
    question:
      'Interventions from child protective services make the situation worse for the child.',
    response: 0,
    invert: true,
    part: 1,
  },

  {
    name: 'societyAcceptingOfChildAdultSex',
    question:
      'If society was more accepting of child-adult-sex, then it would be less harmful.',
    response: 0,
    invert: true,
    part: 1,
  },

  {
    name: 'CPHarmsChild',
    question: 'Downloading child pornography is harmful to the child in it.',
    response: 0,
    invert: false,
    part: 1,
  },

  {
    name: 'MAPTherapyPreventOffending',
    question: 'MAPs need therapy to prevent offending.',
    response: 0,
    invert: false,
    part: 1,
  },

  {
    name: 'adultsSexChildrenImmoral',
    question:
      'Adults who have sexual contact with children are immoral people.',
    response: 0,
    invert: false,
    part: 1,
  },
])
